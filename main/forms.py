from django import forms
from .models import Servicio

class formularioInicio(forms.Form):
    PRIORIDADES = (
        ('alta', 'Prioridad alta'),
        ('media', 'Prioridad media'),
        ('baja', 'Prioridad baja'),
    )
    cedula = forms.IntegerField()
    prioridad = forms.ChoiceField(choices=PRIORIDADES, widget=forms.RadioSelect, initial='baja')
    servicio = forms.ModelChoiceField(queryset=Servicio.objects.all(), initial="Atención al cliente")