from django.shortcuts import render, HttpResponse, redirect
from .forms import formularioInicio
from .models import Persona, Queue, Servicio
from queue import PriorityQueue

# Create your views here.

def inicio(request):
    if request.method == 'POST':
        requestFormulario = formularioInicio(request.POST)
        if requestFormulario.is_valid():
            cedula = requestFormulario.cleaned_data['cedula']
            prioridad = definicion(requestFormulario.cleaned_data['prioridad'])
            servicio = indexServicio(requestFormulario.cleaned_data['servicio'])
            if Persona.objects.filter(cedula=cedula).exists():
                create = Persona.objects.filter(cedula=cedula)[0]
                Queue.objects.create(cedula=create, prioridad=prioridad, servicio=servicio)
            else:
                create = Persona.objects.create(nombreApellido="SIN NOMBRE " + str(cedula), cedula=cedula)
                Queue.objects.create(cedula=create, prioridad=prioridad, servicio=servicio)
    formulario = formularioInicio()
    return render(request, 'inicio.html', {'formulario': formulario})

def visualizar(request):
    list, queue = cola()
    context = {"listado" : list}
    return render(request, 'visualizar.html', context)

def siguiente(request):
    list, queue = cola()
    get = Queue.objects.get(id=queue[0])
    siguiente = (get.cedula, get.servicio, definicionReversa(get.prioridad))
    context = {"siguiente" : siguiente}
    if request.method == 'POST':
        queueFinish = Queue.objects.get(id=queue[0])
        queueFinish.estado = True
        queueFinish.save()
        return redirect(request.path)
    return render(request, 'siguiente.html', context)

def test(request):
    return HttpResponse("Prueba")

def cola():
    q = PriorityQueue()
    getQueue = Queue.objects.filter(estado=False)
    cola = []
    for register in getQueue:
        q.put((int(register.prioridad), str(register.id)))
    while q.empty() == False:
        temp = q.get()
        cola.append(int(temp[1]))
    list = []
    i = 0
    for a in cola:
        get = Queue.objects.get(id=cola[i])
        list.append([get.cedula, get.servicio, definicionReversa(get.prioridad)])
        i += 1
    return list, cola

def definicion(a):
    if a == 'alta':
        return 1
    if a == 'media':
        return 2
    if a == 'baja':
        return 3

def definicionReversa(a):
    if a == 1:
        return 'Prioridad Alta'
    if a == 2:
        return 'Prioridad Media'
    if a == 3:
        return 'Prioridad Baja'

def indexServicio(a):
    get = Servicio.objects.filter(descripcion=a)[0]
    return get
