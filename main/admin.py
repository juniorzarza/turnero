from django.contrib import admin
from main.models import Persona, Servicio, Queue

# Register your models here.

admin.site.register(Persona)
admin.site.register(Servicio)
admin.site.register(Queue)