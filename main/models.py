from django.db import models

# Create your models here.

class Persona(models.Model):
    cedula = models.IntegerField(primary_key=True)
    nombreApellido = models.CharField(max_length=255)
    def __str__(self):
        return self.nombreApellido

class Servicio(models.Model):
    descripcion = models.CharField(max_length=255)
    def __str__(self):
        return self.descripcion

class Queue(models.Model):
    cedula = models.ForeignKey(Persona, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    prioridad = models.IntegerField(default=3)
    estado = models.BooleanField(default=False)
    hora = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f"{self.cedula} - Servicio: {self.servicio} - Prioridad: {self.prioridad} - Estado: {self.estado} - Hora: {self.hora}"